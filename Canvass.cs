﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace ASEProgrammingLanguageEnvron
{
    /// <summary>
    /// The canvass calss holds the turtle information that is displayed on the picturebox
    ///<br> which is on the form in repsonse to the programming language environment</br>
    /// </summary>
    
    public class Canvass
    {
        /// <summary>
        /// The grapihcs object provides methods for drawing objects on the picturebox.
        /// </summary>
        public Graphics g;
        /// <summary>
        /// // Provides bitmap overlay to draw a dot/cursor onto picturebox.
        /// </summary>
        //public Graphics gOverlay;
        /// <summary>
        ///Pen Defines object used to draw shapes and lines.
        /// </summary>
        public Pen pen; 
        /// <summary>
        /// Integer x and y used for pen position.
        /// </summary>
        public  int xPos, yPos;  

        public List<Shape> Shapes = new List<Shape>();
        /// <summary>
        /// Canvass constructor has parameters
        ///<br> Sets x and y position to origin/initial postion of 0</br>
        ///<br> Initialises pen object with specific colour and its width</br>
        /// </summary>
        /// <param name="g"> graphics context used to draw shapes.</param>
        /// <param name="gOverlay"> graphics context used to draw cursor on overlaying bitmap.</param>
        public Canvass(Graphics g)
        {
            this.g = g;
            xPos = 0; 
            yPos = 0;
            pen = new Pen(Color.Black, 2); 
            g.Clear(Color.Transparent);
            
        }

        public void AttachShape(Shape s)
        {
            s.ShapePen = new Pen(Color.FromName(this.pen.Color.Name));
            s.x = this.xPos;
            s.y = this.yPos;
            this.Shapes.Add(s);
            this.Refresh();
        }
        public void Refresh()
        {
            g.Clear(Color.Transparent);

            foreach (var shape in this.Shapes)
            {
                shape.draw(this.g, shape.ShapePen);
            }
            float temp = pen.Width;
            pen.Width = 6;
            Brush b = new SolidBrush(Color.Red); //// sets colour of dot to red.
            g.FillEllipse(b, this.xPos - pen.Width / 2, this.yPos - pen.Width / 2, pen.Width, pen.Width);
            pen.Width = temp; // cursor is drawn as a small dot
        }
        
        public void ChangeColor(string color)// method to change initial pen colour 

        {
            pen.Color = Color.FromName(color);// user types new colour to draw shapes in different colours. 
        }
        public void UpdatePosition(int x, int y) //Updates the dot/pen position
        {
            
            xPos = x;//sets initial x position to updated position
            yPos = y; //sets initial y position to updated position
            this.Refresh();
        }

        public void MoveTo(int[] param) //parses the actual data parameter type
                                        //required to move pen from one position to another.
        {
            int x, y;
            x = param[0];
            y = param[1];
            UpdatePosition(x, y); // updates pen position to position specified by user.
        }
        public void Clear() // method to clear entire drawing on picturebox
        {
            this.Shapes.Clear();
            this.Refresh();

        }
        
    }

        // throws an error message of unknown command if an additional command
        // is inputed on to program

        /*
         * class used to store syntax error info and gets thrown when line isn't syntantically correct
         */
        public class ParamException : Exception
    {
        public string SyntaxErrorType { get; set; } //gives/stores the  syntax error type
        

        public ParamException(string errorType)
        {
            SyntaxErrorType = errorType;
            
        }

        //gets the string that is going to be displayed to the user about the syntax error.
        
    }
}


