﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASEProgrammingLanguageEnvron
{
   public class Variable
    {
        /// <summary>
        /// Declares a var method that takes string and integers.
        /// </summary>
        private Dictionary<string, int> vars = new Dictionary<string, int>();

        /// <summary>
        /// Write variable method to be used to enter variable name and variable value
        /// </summary>
        /// <param name="name">name parameter to be used t enter variable name</param>
        /// <param name="value">value parameter used to enter variable value</param>
        public void WriteVariable(string name, int value)
        {
            if (this.vars.ContainsKey(name))
            {
                this.vars[name] = value;
                return;
            }
            this.vars.Add(name, value);
        }
        /// <summary>
        /// Boolean to check if user entered variable name
        /// </summary>
        /// <param name="name"> (param=name) used to check for variable name</param>
        /// <returns></returns>
        public bool Contains(string name)
        {
            return this.vars.ContainsKey(name);
        }
        /// <summary>
        /// Methos to get the variable value using the name
        /// </summary>
        /// <param name="name"> param=name used ot get variable value</param>
        /// <returns>Returns value when user enters name.</returns>
        
        public int GetVariableValue(string name)
        {
            if (this.vars.ContainsKey(name))
            {
                return this.vars[name];
            }

            return 0;
        }
        /// <summary>
        /// Clear Method to clear screen when after user draws on surface using variable name and value.
        /// </summary>
        public void Clear()
        {
            this.vars.Clear();
        }
    }
}
    
