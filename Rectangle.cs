﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASEProgrammingLanguageEnvron
{
    /// <summary>
    /// Rectangle class which extends Shape and inherits from shape class.
    /// <br> Takes 2 integers height and width which user specifies in command line to draw a rectangle.</br>
    /// </summary>
   public class Rectangle : Shape
    {
        private int Width;

        private int Height;
        /// <summary>
        /// Rectangle constuctor with internal modifier for access allowance.
        /// </summary>
        /// <param name="pars">pars parameter used when passing rectangle commands, uses if statement to check that user entered to parameters.</param>
        internal Rectangle(int [] pars)
        {
            if (pars.Length != 2)
            {
                throw new ParamException("bad params");
            }
            if (pars.Length != 2)
            {
                throw new ParamException("bad params");
            }
            this.Width = pars[0];
            this.Height = pars[1];
        }
        ///
        /// <summary>
        /// Rectangle constructor, inherits from base class(shape). 
        /// <br>Declares new 2 integer parameters width and height</br>
        /// </summary>
        /// <param name="width">param width used to get and set width of rectangle</param>
        /// <param name="height">param height used to get and set height of rectangle.</param>


        public Rectangle(int Width, int Height) : base()
        {
            this.Width = Width;
            this.Height = Height;
        }

        /// <summary>
        /// Draw method overrides base class to draw line with specific x and y coordinates suitable for line.
        /// <br></br>
        /// <br>(g.DrawRectangle ) uses a pen to draw a rectangle specified by x and y as well as width and height values.</br>
        /// </summary>
        /// <param name="g">g parameter takes a Graphics context to privide drawing surface.</param>
        /// <param name="p">p param defines an object used to draw rectangle onto picturebox.</param>
        public override void draw(Graphics g, Pen p)
        {
            g.DrawRectangle(p, this.x +this.Width , this.y +this.Height , this.Width, this.Height);
        }
    }
}
