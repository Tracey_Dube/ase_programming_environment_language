﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASEProgrammingLanguageEnvron
{
    /// <summary>
    /// Circle class extends Shapes, inherits stuff from Shape Class.
    /// <br></br>
    /// Circle is the child of class Shape.
    /// <br></br>
    /// Declares int radius used to draw circle
    /// </summary>
   public class Circle : Shape
    {
        int radius;

        /// <summary>
        /// Circle Constructor with internal modifier for access allowance.
        /// </summary>
        /// <param name="pars">pars parameter used when passing circle commands, uses if statement to check for valid parameters when drawing circle </param>
        internal Circle(int[] pars)
        {
            if (pars.Length != 1)
            {
                throw new ParamException("bad params");
            }

            this.radius = pars[0];

        }
        /// <summary>
        /// Circle constructor, inherited from base class.
        /// </summary>
        /// <param name="radius"> Used to specify size of cirlce.</param>
        public Circle(int radius) : base()
        {
            this.radius = radius;

        }

       

        /// <summary>
        ///  Overrides Shape class method draw, inherited by circle shapes and add stuff specific to circle.
        /// <br></br>
        ///(g.drawEllipse) uses a pen, from current x and y and draws circle around a cursor.
        ///<br></br>
        ///(g.fillEllipse) fills the interior of circle using brush with specified colour.
        /// </summary>
        /// <param name="g">param g, Graphics context used to draw circle on surface.</param>
        /// <param name="p">p parameter defines pen object with colour black and width of 2 used to draw circle</param>


        public override void draw(Graphics g, Pen p)
        {
            SolidBrush b = new SolidBrush(Color.DarkGray);
            g.DrawEllipse(p, this.x-radius, this.y-radius, (this.radius * 2), (this.radius * 2));
            g.FillEllipse(b, this.x-radius, this.y-radius, this.radius * 2, (this.radius * 2));
            
        }

        

        /// <summary>
        /// To String method calls ToString in shape.
        /// </summary>
        /// <returns>Returns x and y position as well as specified radius of circle.</returns>
        public override string ToString()
        {
            return base.ToString() + " "+this.radius;
        }

       
    }
}
