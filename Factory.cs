﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASEProgrammingLanguageEnvron
{
    /// <summary>
    /// Shape Factory used to generate objects of concrete classes.
    /// </summary>
   public class ShapeFactory
    {
        /// <summary>
        /// Shape factory to generate/create new shapes.
        /// </summary>
        /// <param name="commandtype">command_type string param used pass command,<br>made to lower to make commands case insetivite and trim to remove any trailing spaces</br>  </param>
        /// <param name="param">param parameter to pass specific shape commands e.g radius for circle</param>
        /// <returns> Returns/draws shape (if commandtype is (shape name)) and user typed in commands in command line </returns>
        public Shape GetShape(string commandtype, int[] param)
        {
            commandtype = commandtype.ToLower().Trim();
            if (commandtype.Equals("square"))
            {
                return new Square(param);
            }

            else if (commandtype.Equals("line"))
            {
                return new Line(param);
            }
            else if (commandtype.Equals("circle"))
            {
                return new Circle(param);
            }
            else if (commandtype.Equals("rectangle"))
            {
                return new Rectangle(param);
            }
           return null;
            throw new ApplicationException("Factory error:no such class");
            
        }
    }
}
