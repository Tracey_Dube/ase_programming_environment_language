﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using System.Linq;
/// <summary>
/// ASE Programming language Environment(turtle) program for drawing different shapes including
/// <br>Circle, Rectangle, Square and Line.</br>
/// @ Author Tracey Dube.
/// </summary>
namespace ASEProgrammingLanguageEnvron
{
    /// <summary>
    /// Form1 extends form.
    /// <br>Declares a constant integer to hold xSize for bitmap</br>
    /// Declares a constant integer to hold ysize for bitmap.
    /// <br>Initialises Shapefactory Object Sf.</br>
    /// Initialises Bitmaap Object Outputbmp which passes xSize and ySize, Bitmap is used to draw shapes on, displayed in picture box.
    /// <br>Initialises Overlaybmp used to draw a dot /cursor for moveto command which will be displayed in the picturebox onto bitmap. </br>
    /// Initialises my Canvass Class object to which will be used to call Canvass class that holds command methods and will be called when user runs program
    /// </summary>

    public partial class Form1 : Form
    {
        public Variable vars = new Variable();
        public const int xSize = 600;
        public const int ySize = 600;
        public ShapeFactory sf = new ShapeFactory();
        public Bitmap Outputbmp = new Bitmap(xSize, ySize);
        public Bitmap Overlaybmp = new Bitmap(xSize, ySize);
        public Canvass MyCanvass;
        /// <summary>
        /// Form constructor, holds initializeComponent which is an automatically generated method.
        ///<br>>Holds myCanvass objectwhich passes a reference to Outputbmp to create new Graphics from Bitmap and returns new graphics from specified Image.</br
        /// </summary>
        public Form1()
        {
            InitializeComponent();
            this.LoadBtn.Click += new System.EventHandler(this.LoadBtn_Click);

            MyCanvass = new Canvass(Graphics.FromImage(Outputbmp));
            this.MyCanvass.Refresh();
        }

     /// <summary>
     /// Boolean method to check if variable( value= numeric)
     /// </summary>
     /// <param name="value">param name=value to take numeric variable</param>
     /// <returns>Return (value=numeric)</returns>
        public bool IsNumeric(string value)
        {
            return value.All(char.IsNumber);
        }


        /// <summary>
        /// Parseline method to parse commands which holds string parameter.
        /// <br>Declares variable trim is equals line.trim to tidy line up and get rid of any trailing spaces when user enters command. 
        /// <br>Uses tolower to make the commands case insentive. </br>
        ///  <br></br>
        /// Declares array of string to split line into substrings based on the size of the array.
        /// <br>Declares command string as array index 0, this is the command entered by user e.g moveto command.</br>
        /// <br>Declares a list parameters that can be accessed by index and can be manipulated like breaking them into parts/chunks.</br>
        /// Tests for valid commands using if statements  for commnads inside canvass class and a for loop for commands which are from different classes for inheritence and are called here.
        /// </summary>
        /// <param name="line"> line param used to Split commands and parameters in commands linee  </param>
        public void ParseLine(String line)
        {
            var trim = line.Trim().ToLower(); 
            string[] tokens = trim.Split(' ');
            string command = tokens[0];  
            List<int> paramsList = new List<int>();

            if (command.Equals("color") && tokens.Length > 1)
            {
                MyCanvass.ChangeColor(tokens[1]);
                return;
            }
            foreach (var tokenR in tokens)
            {
                if (tokenR != command)
                {
                    if (this.IsNumeric(tokenR))
                    {
                        paramsList.Add(int.Parse(tokenR));
                    }
                    else if (this.vars.Contains(tokenR))
                    {
                        int val = vars.GetVariableValue(tokenR);
                        paramsList.Add(val);
                    }
                }
            }

            if (command.Equals("moveto") && tokens.Length > 1)
            {
                MyCanvass.MoveTo(paramsList.ToArray());
            }
            else if (command.Equals("clear"))
            {

                MyCanvass.Clear();
            }
            else if (tokens.Length > 1 && tokens[1].Equals("="))
            {
                this.vars.WriteVariable(tokens[0], int.Parse(tokens[2]));
            }
            else if (tokens.Length > 1)
            {
                Shape temp = null;
                try
                {
                    temp = this.sf.GetShape(command, paramsList.ToArray());
                }
                catch
                {
                    ErrorOutputArea.Text = "Bad Shape";
                }
                this.MyCanvass.AttachShape(temp);
            }
            else
            {
                ErrorOutputArea.Text = "Bad Console Command";
            }
            Refresh();
        }


        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {

        }
        /// <summary>
        /// Keydown event used to detect what user types on command line and gets and sets text associate with parseline.
        /// <br> In this case when user presses enter after typing in shapes are drawn onto picturebox or errors messages are displayed onto error outputbox  </br>
        /// On the console a message 'enter key pressed' is displayed.
        /// </summary>
        /// <param name="sender">sender parameter is the instance of the object which raised the event. 
        ///The code for the class which raises the event is responsible for passing the correct object to the handler</param>
        /// <param name="e">e parameter provides data for the control keydown. </param>

        public void CommandLine_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                ParseLine(CommandLine.Text);
                Console.WriteLine("return pressed!");
            }
            
            Refresh(); 
        }

        /// <summary>
        /// DrawArea_Paint method calls the Paint Event Args to paint(draw) shapes onto picture box which in this case is named drawing area.
        ///<br></br> 
        /// (Graphics g= e.Graphics) gets the graphics used to paint(draw)
        /// <br></br>
        /// (DrawingArea.BackGroundImage=outputbmp) sets the background image for shapes to be drawn onto picturebox.
        /// <br></br>
        /// (g.DrawImageUnscaled) draws an overlay bitmap using its physical size using coordinates x and y.
        /// </summary>
        /// <param name="sender">sender parameter is the instance of the object which raised the event </param>
        /// <param name="e"> e parameter provides data control for paint event.</param>

        private void DrawingArea_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics; 
            DrawingArea.BackgroundImage = Outputbmp;
            g.DrawImageUnscaled(Overlaybmp, 0, 0);



        }

        private void DrawingArea_Click(object sender, EventArgs e)
        {

        }
        private void LoadBtn_Click(object sender, EventArgs e)
        {
            //OutputWindow.LoadFile(@"C: \uni\Program.txt", RichTextBoxStreamType.PlainText);
            //if (OpenFileDialog.S() == DialogResult.OK)
            //{
            //   OutputWindow.LoadFile(OpenFileDialog.FileName, RichTextBoxStreamType.RichText);
            // }
            OpenFileDialog file_open = new OpenFileDialog();

            file_open.Filter =
            "Rich Text File (*.rtf)|*.rtf| Plain Text File (*.txt)|*.txt";
            file_open.FilterIndex = 1;
            file_open.Title = "Open text or RTF file";
            file_open.FilterIndex = 1;
            file_open.Title = "Open text or RTF file";

            RichTextBoxStreamType stream_type;
            stream_type = RichTextBoxStreamType.RichText;
            if (DialogResult.OK == file_open.ShowDialog())
            {
                //Richtext 02_04: Set the correct stream type 
                //(Rich text or Plain text?)
                if (string.IsNullOrEmpty(file_open.FileName))
                    return;
                if (file_open.FilterIndex == 2)
                    stream_type = RichTextBoxStreamType.PlainText;
                //Richtext 02_05: Open the content of the selected file
                OutputWindow.LoadFile(file_open.FileName, stream_type);
            }
        }
        private void ErrorOutputArea_Click(object sender, EventArgs e)
        {

        }

        private void RunButton_Click(object sender, EventArgs e)
        {

            foreach (var line in OutputWindow.Lines)
            {
                ParseLine(line);
            }

        }

        private void CheckSyntaxBtn_Click(object sender, EventArgs e)
        {



        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void SaveBtn_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveDlg = new SaveFileDialog();
            string filename = "";

            //Richtext 01_02: Set the Filters for the save dialog.
            //Don't include space when typing *.ext. Because space is 
            //treated as extension
            saveDlg.Filter =
            "Rich Text File (*.rtf)|*.rtf|Plain Text File (*.txt)|*.txt";
            saveDlg.DefaultExt = "*.rtf";
            saveDlg.FilterIndex = 1;
            saveDlg.Title = "Save the contents";

            //Richtext 01_03: Show the save file dialog
            DialogResult retval = saveDlg.ShowDialog();
            if (retval == DialogResult.OK)
                filename = saveDlg.FileName;
            else
                return;

            RichTextBoxStreamType stream_type;
            if (saveDlg.FilterIndex == 2)
                stream_type = RichTextBoxStreamType.PlainText;
            else
                stream_type = RichTextBoxStreamType.RichText;

            //Richtext 01_05: Now its time to save the content
            OutputWindow.SaveFile(filename, stream_type);

        }
    }
}

