﻿namespace ASEProgrammingLanguageEnvron
{
    using System;

    public class SyntaxException : Exception
    {
        public string SyntaxErrorType { get; set; } //gives/stores the  syntax error type
        public string Section { get; set; } //Stores section of code that caused the syntax error 
        public int Line { get; set; } // stores integer that reflects the line of the syntax error
        public string FullLine { get; set; } //full line that the syntax error exists from

        public SyntaxException(string section, string errorType)
        {
            this.SyntaxErrorType = errorType;
            this.Section = section;
        }

        //gets the string that is going to be displayed to the user about the syntax error.
        public string GetString()
        {
            return $"Line:{this.Line} Error:{this.SyntaxErrorType} Code Section:{this.Section}";
        }
    }
}