﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace ASEProgrammingLanguageEnvron
{
    /// <summary>
    /// Square class to draw square, extends shapes (inherits Shape class)
    /// </summary>
    class Square : Shape
    {
        private int Size;
        /// <summary>
        /// constructor with internal access modifier for access allowance from other classes. 
        /// </summary>
        /// <param name="pars"></param>
        internal Square(int[] pars)
        {
            if (pars.Length != 1)
            {
                throw new ParamException("bad params");
            }
            this.Size = pars[0];
        }
        public Square(int size):base()
        {
            this.Size = size;
        }
        public override void draw(Graphics g, Pen p)
        {
            g.DrawRectangle(p, this.x - (this.Size /2), this.y - (this.Size / 2), this.Size, this.Size);
        }
    }

    
    
}
