﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace ASEProgrammingLanguageEnvron
{
    
    public abstract class Shape
    {
        
        public int x, y = 0;
        
        public Pen ShapePen = new Pen(Color.Black);
        /// <summary>
        /// Shape's constructor that takes three parameters
        /// <br></br>
        /// Will be called by child classes through base statement.
        /// </summary>
        ///<param name= "colour"> Shape's colour</param>
        /// <param name="x">its x position </param>
        /// <param name="y">its y position</param>
        public Shape()
        {
            
        }
        /// <summary>
        /// Method to draw shapes
        /// <br></br>
        /// has parameter g, graphics context for drawing objects.
        /// </summary>
        /// <param name="g"></param>
        public abstract void draw(Graphics g, Pen p);

        /// <summary>
        /// Method to calculate shapes' area
        /// </summary>
        /// <returns> returns area as double</returns>
        //public abstract double calcArea();

        /// <summary>
        /// Method to calculate shapes' perimeter
        /// </summary>
        /// <returns> returns perimeter as double</returns>
        //public abstract double calcPerimeter();

        /// <summary>
        /// set declared as virtual so it can be overrided by a more specific child class. 
        /// <br>Declared in shape class so </br>
        /// that it can be called by child class to do generic stuff.
        /// </summary>
        /// <param name="colour"></param>
        /// <param name="list">use of param keyword</param>
        ///provides a variable parameter list to cope with some shapes having more information
        ///than others. 
        //public virtual void set(Color colour, params int[] list)
        //{
        //    this.colour = colour;
        //    this.x = list[0];
        //    this.y = list[1];
        //}
        /// <summary>
        /// To string method to return string that represents Shape instance.
        /// </summary>
        /// <returns> Returns value of x and y position .</returns>
        public override string ToString()
        {
            return base.ToString() + " " + this.x + "" + this.y + ":";
        }
    }
    
}
