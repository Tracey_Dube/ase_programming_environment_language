﻿using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
namespace ASEProgrammingLanguageEnvron
{
   
    /// <summary>
    /// Class to draw Line onto picturebox from iniatial x and y position to x and y position specified by user.
    /// </summary>
  public  class Line : Shape
    {
        private float ToX;

        private float ToY;
        /// <summary>
        /// line constructor with internal access modifier for access allowance. 
        /// </summary>
        /// <param name="pars">pars parameter used to parse line commands and uses if statement to check for valid parameters when drawing line.</param>
        internal Line(int[] pars)
        {
            if (pars.Length != 2)
            {
                throw new ParamException("bad params");
            }
            this.ToX = pars[0];
            this.ToY = pars[1];
        }
        /// <summary>
        /// Draw method overrides base class to draw line with specific x and y coordinates suitable for line.
        /// <br></br>
        /// <br>(g.DrawLine ) uses a pen to draw a line connecting the two points specified by the x and y coordinates</br>
        /// </summary>
        /// <param name="g">g parameter takes a Graphics context to privide drawing surface.</param>
        /// <param name="p">p param defines an object used to draw shapes and line.</param>
        public override void draw(Graphics g, Pen p)
        {
            g.DrawLine(p, this.x, this.y,this.ToX, this.ToY);
        }
    }
}