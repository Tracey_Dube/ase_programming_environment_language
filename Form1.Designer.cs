﻿using System;

namespace ASEProgrammingLanguageEnvron
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.OutputWindow = new System.Windows.Forms.RichTextBox();
            this.CommandLine = new System.Windows.Forms.TextBox();
            this.DrawingArea = new System.Windows.Forms.PictureBox();
            this.RunButton = new System.Windows.Forms.Button();
            this.CheckSyntaxBtn = new System.Windows.Forms.Button();
            this.ErrorOutputArea = new System.Windows.Forms.TextBox();
            this.LoadBtn = new System.Windows.Forms.Button();
            this.SaveBtn = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.DrawingArea)).BeginInit();
            this.SuspendLayout();
            // 
            // OutputWindow
            // 
            this.OutputWindow.Location = new System.Drawing.Point(18, 9);
            this.OutputWindow.Name = "OutputWindow";
            this.OutputWindow.Size = new System.Drawing.Size(532, 465);
            this.OutputWindow.TabIndex = 0;
            this.OutputWindow.Text = "";
            this.OutputWindow.TextChanged += new System.EventHandler(this.richTextBox1_TextChanged);
            // 
            // CommandLine
            // 
            this.CommandLine.Location = new System.Drawing.Point(12, 543);
            this.CommandLine.Name = "CommandLine";
            this.CommandLine.Size = new System.Drawing.Size(278, 26);
            this.CommandLine.TabIndex = 1;
            this.CommandLine.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CommandLine_KeyDown);
            // 
            // DrawingArea
            // 
            this.DrawingArea.BackColor = System.Drawing.Color.LightSeaGreen;
            this.DrawingArea.Location = new System.Drawing.Point(559, 9);
            this.DrawingArea.Name = "DrawingArea";
            this.DrawingArea.Size = new System.Drawing.Size(833, 793);
            this.DrawingArea.TabIndex = 2;
            this.DrawingArea.TabStop = false;
            this.DrawingArea.Click += new System.EventHandler(this.DrawingArea_Click);
            this.DrawingArea.Paint += new System.Windows.Forms.PaintEventHandler(this.DrawingArea_Paint);
            // 
            // RunButton
            // 
            this.RunButton.Location = new System.Drawing.Point(16, 484);
            this.RunButton.Name = "RunButton";
            this.RunButton.Size = new System.Drawing.Size(98, 40);
            this.RunButton.TabIndex = 3;
            this.RunButton.Text = "Run";
            this.RunButton.UseVisualStyleBackColor = true;
            this.RunButton.Click += new System.EventHandler(this.RunButton_Click);
            // 
            // CheckSyntaxBtn
            // 
            this.CheckSyntaxBtn.Location = new System.Drawing.Point(150, 486);
            this.CheckSyntaxBtn.Name = "CheckSyntaxBtn";
            this.CheckSyntaxBtn.Size = new System.Drawing.Size(85, 38);
            this.CheckSyntaxBtn.TabIndex = 4;
            this.CheckSyntaxBtn.Text = "Syntax";
            this.CheckSyntaxBtn.UseVisualStyleBackColor = true;
            this.CheckSyntaxBtn.Click += new System.EventHandler(this.CheckSyntaxBtn_Click);
            // 
            // ErrorOutputArea
            // 
            this.ErrorOutputArea.Location = new System.Drawing.Point(559, 808);
            this.ErrorOutputArea.Multiline = true;
            this.ErrorOutputArea.Name = "ErrorOutputArea";
            this.ErrorOutputArea.Size = new System.Drawing.Size(596, 87);
            this.ErrorOutputArea.TabIndex = 5;
            this.ErrorOutputArea.Click += new System.EventHandler(this.ErrorOutputArea_Click);
            // 
            // LoadBtn
            // 
            this.LoadBtn.Location = new System.Drawing.Point(286, 487);
            this.LoadBtn.Name = "LoadBtn";
            this.LoadBtn.Size = new System.Drawing.Size(75, 37);
            this.LoadBtn.TabIndex = 6;
            this.LoadBtn.Text = "Load";
            this.LoadBtn.UseVisualStyleBackColor = true;
            this.LoadBtn.Click += new System.EventHandler(this.LoadBtn_Click);
            // 
            // SaveBtn
            // 
            this.SaveBtn.Location = new System.Drawing.Point(399, 486);
            this.SaveBtn.Name = "SaveBtn";
            this.SaveBtn.Size = new System.Drawing.Size(78, 38);
            this.SaveBtn.TabIndex = 7;
            this.SaveBtn.Text = "Save";
            this.SaveBtn.UseVisualStyleBackColor = true;
            this.SaveBtn.Click += new System.EventHandler(this.SaveBtn_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1404, 942);
            this.Controls.Add(this.SaveBtn);
            this.Controls.Add(this.LoadBtn);
            this.Controls.Add(this.ErrorOutputArea);
            this.Controls.Add(this.CheckSyntaxBtn);
            this.Controls.Add(this.RunButton);
            this.Controls.Add(this.DrawingArea);
            this.Controls.Add(this.CommandLine);
            this.Controls.Add(this.OutputWindow);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.DrawingArea)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

       



        #endregion

        private System.Windows.Forms.RichTextBox OutputWindow;
        private System.Windows.Forms.TextBox CommandLine;
        private System.Windows.Forms.PictureBox DrawingArea;
        private System.Windows.Forms.Button RunButton;
        private System.Windows.Forms.Button CheckSyntaxBtn;
        private System.Windows.Forms.TextBox ErrorOutputArea;
        private System.Windows.Forms.Button LoadBtn;
        private System.Windows.Forms.Button SaveBtn;
    }
}

